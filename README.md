# Ansible Collection - symaxion.common

**A collection of Ansible roles, plugins and playbooks which are common between
all of my Ansible projects.**

#### Usage

Add this to your `requirements.yml`:
```yaml
collections:
- name: git+https://gitlab.com/Symaxion/ansible-common.git
  version: '0.1.0'
  type: git
```

Then run:
```bash
ansible-galaxy install -r requirements.yml
```

## Vars Plugins

### `symaxion.common.merge_host_group_vars`

As of Ansible 2.10, `hash_behaviour = merge` is deprecated. This is a vars
plugin which replaces the default builtin `host_group_vars` plugin with one
that always merges dictionaries.

> 📣 **Important!**  
> This plugin has a couple key differences from the `hash_behaviour = merge`
> setting. Specifically, this plugin only applies to *inventory* variables,
> while the merge behaviour applied to *all* variables. So for example, setting
> this in inventory:
>
> ```yaml
> colors:
>   red:    '#FF0000'
>   green:  '#00FF00'
> ```
>
> and then running:
>
> ```yaml
> - set_fact:
>     colors:
>       blue: '#0000FF'
> ```
>
> will **append** `blue` to the `colors` hash on `hash_behaviour = merge` but
> will **overwrite** the `colors` hash when using the plugin.

#### Usage

Add this to your `ansible.cfg`:
```ini
[defaults]
vars_plugins_enabled = symaxion.common.merge_host_group_vars
```
